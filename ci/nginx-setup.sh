#!/bin/bash -e

# Make the site's files directory writable by Nginx.
mkdir -p $DOCROOT/sites/default/files
chown -R nginx $DOCROOT/sites/default/files

# Deploy our vhosts.
cp ci/nginx-site.conf /etc/nginx/conf.d/$SITE_URL.conf
ln -sf /etc/nginx/conf.d/$SITE_URL.conf /etc/nginx/sites-enabled/$SITE_URL.conf

# Remove the default vhost
rm /etc/nginx/sites-enabled/nginx-site-default.conf

# Start the web server
cp /etc/supervisor/php-fpm.conf /etc/supervisor/conf.d/
cp /etc/supervisor/supervisord-nginx-fpm.conf /etc/supervisor/conf.d/nginx-fpm.conf
service supervisor start || /bin/true
