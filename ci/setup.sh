#!/bin/bash -e

# Install system packages to allow builds and sending notifications of test results.
echo "Installing required system packages."
apt update -yqq
apt install -yqq make

echo "CWD: $PWD"
echo "Symlinking project path into /opt/behat-drupal-context: $CI_PROJECT_PATH"
ln -s $CI_PROJECT_DIR/ /opt/behat-drupal-context

# Disable xdebug
disable_xdebug

# Setup steps to be run inside CI docker image at CI time.
echo "Setting up site '$SITE_URL'."

# Update docroot with latest changes, clean up codebase.
rm -rf "$CODEBASE_PATH"
ln -s `pwd` "$CODEBASE_PATH"
cd "$CODEBASE_PATH"

# Update PHP version
if [ -n "$DDEV_PHP_VERSION" ] ; then
    echo "Updating PHP version to ${DDEV_PHP_VERSION}."
    update-alternatives --set php /usr/bin/php${DDEV_PHP_VERSION}
    ln -sf /usr/sbin/php-fpm${DDEV_PHP_VERSION} /usr/sbin/php-fpm
    export PHP_INI=/etc/php/${DDEV_PHP_VERSION}/fpm/php.ini
fi

# Bootstrap Drumkit.
source d

echo "Building the codebase."
make build

echo "Installing the site."
make install

echo "Configuring and starting web server."
ci/nginx-setup.sh

echo "Ensuring that base URL resolves."
echo `hostname -I` $SITE_URL >> /etc/hosts

echo "Checking that a site has been installed."
cd test && ./bin/drush --uri=$SITE_URL status
