# Development README for behat-drupal-context

`behat-drupal-context` is a developer's tool, built by Drupal developers.
We've taken a test-driven approach to building the library, starting with a
DDEV "test app" (this `test` directory) that we run our Behat steps against to
validate they work as expected. We thus have a small Drupal site embedded in
the package.

**NB** Please ensure you clone this git repository with `git clone --recursive`
to ensure you have the submodules you need (or do a `git submodule update --init --recursive`)

This project is built on [Drumkit](https://drumk.it) to orchestrate the
localdev toolkit. This means we have default `make` targets that serve as
shortcuts to our most common operations:

* `git clone --recursive` -
* `source d` - Bootstrap Drumkit
* `make start` - start up the DDEV containers that provide the test application
* `make build` - run a `composer install` on the codebase, to build the site's vendor/ dir
* `make install` - install the test app (run `drupal site:install` and set up admin user/password as `dev`/`pwd/
* `make tests` - run the full set of test suites on the codebase.
* `make` - show all the possible targets available.

**NB** The main `Makefile` in the root of the project is the entrypoint to all
of Drumkit's functionality. You need to be in this folder to run the above
targets.

We really like Drumkit targets as "shortcuts", both because they simplify
common workflows, but because *we are not tied to them*, as we can always
access the tools directly, find out what's under the hood with a simple `make
-n install`. It's also easy to extend these targets or add our own, by just
using the standard Makefile syntax:

```Makefile
# First the target, then a list of dependencies (after the colon), to run first
do-the-whole-thing: start build install tests
  # After the dependent targets run, do this series of `bash`-y steps
  echo "I did the WHOLE thing!"
```

## Requirements

Most of the project is self-contained, but there are a handful of baseline
requirements:

* We recommend a modern Unix-like operating system (eg. Ubuntu or OSX). On Windows OS, we suggest using [Virtualbox](/tutorials/getting-started/virtualbox) to get a working Linux system.

**Debian variants:**

```bash
sudo apt install git make

# DDEV
curl -LO https://raw.githubusercontent.com/drud/ddev/master/scripts/install_ddev.sh && bash install_ddev.sh
mkcert -install

**MacOS:**

```bash
brew install git make

