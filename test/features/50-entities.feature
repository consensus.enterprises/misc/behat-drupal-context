@admin @api @entities
Feature: Test creating any entities.
  In order to test Drupal applications with entities
  As a Developer
  I need Behat steps to create Entities of a given Type from a text table in my test step.

  Background: I am an administrator
    When I am logged in as an "Administrator"

  Scenario: A view listing entities exists.
    Given content in the "abc" bundle of the "saurabh" entity:
      | title           | field_eck_example_field1 | field_eck_example_field2    |
      | abc title       | this is some text        | this is another field value |
      | abc title1      | this is some text        | this is another field value |
      | abc title2      | this is some text        | this is another field value |
      And content in the "xyz" bundle of the "saurabh" entity:
      | title           |
      | xyz title       |
      And I run "bin/drush en -y entity_example2"
      And "entity_example2" entities of the "entity_example2_bundle1" bundle:
      | title                              |
      | Entity Example 2 Bundle 1 instance |

     When I go to "/admin/reports/entities"
     Then I should see "abc title"
      And I should see "eck_example_field1"
      And I should see "eck_example_field2"
      And I should see "this is some text"
      And I should see "this is another field value"
      And I should see "xyz title"
     When I go to "/admin/content/entity_example2"
     Then I should see "Entity Example 2 Bundle 1 instance"

  Scenario: I can create entity from a file.
    Given I run "ls features/entities/generic-entities.table.txt"
     Then I should get:
     """
     features/entities/generic-entities.table.txt
     """
    Given content in the "abc" bundle of the "saurabh" entity from "features/entities/generic-entities.table.txt"
     When I go to "/admin/reports/entities"
     Then I should see "eck_example_field1"
      And I should see "eck_example_field2"
      And I should see "new text 131313"
      And I should see "another new text 4343434343"

  Scenario: I can create nodes which reference generic entities
    Given content in the "abc" bundle of the "saurabh" entity from "features/entities/generic-entities.table.txt"
      And "simple_entity_nodetype" content:
      | title              | status | created            | author  | field_abc_or_xyz_entity     |
      | Simple Entity 1    | 1      | 2023-02-02 09:00am | user101 | pqr title                   |
      | Simple Entity 2    | 1      | 2023-02-02 09:00am | user101 | pqr title                   |
      And "simple_entity_nodetype" content from "features/entities/node-entity.table.txt"
     When I go to "admin/content"
     Then I should see "Simple Entity 1"
     When I click "Simple Entity 1"
     Then I should see "Simple Entity 1" in the "Page Title" region
      And I should see "Simple Entity"
      And I should see "pqr title"
      And I should see "eck_example_field1"
      And I should see "new text 131313"
      And I should see "eck_example_field2"
      And I should see "another new text 4343434343"
     When I go to "admin/content"
     Then I should see "Simple Entity 2"
     When I click "Simple Entity 2"
     Then I should see "Simple Entity 2" in the "Page Title" region
      And I should see "pqr title"

  Scenario: After the scenario creating entities finishes, the entities don't exist anymore.
    Given I am logged in as a "Administrator"
     When I go to "/admin/reports/entities"
     Then I should not see "new text 131313"
      And I should not see "this is another field value"
      And I should not see "another new text 4343434343"
