@admin @api @paragraphs
Feature: Test creating Paragraph entities.
  In order to test Drupal applications with Paragraphs
  As a Developer
  I need a Behat step to create paragraphs from a text table in my test step.

  Scenario: A view listing paragraphs exists.
    Given I am logged in as an "Administrator"
    When I go to "/admin/reports"
    Then I should see "Administrative listing of Paragraph entities"
    When I go to "/admin/reports/paragraphs"
    Then I should see "Paragraphs Admin"

  Scenario: Paragraph type exists
    Given I am logged in as an "Administrator"
     When I go to "admin/structure/paragraphs_type"
     Then I should not see "There are no Paragraphs types yet."
      And I should see "Simple Paragraph"
      And I should see "This is a simple paragraph type to be used as an example."
     When I go to "admin/structure/paragraphs_type/simple_paragraph/fields"
     Then I should see "Paragraph Title"
      And I should see "field_simple_paragraph_title"
      And I should see "Paragraph Description"
      And I should see "field_simple_paragraph_desc"

  Scenario: I can create paragraphs using old-style tables.
    Given a "simple_paragraph" paragraph named "example_paragraph_1":
      | field_simple_paragraph_title | Example Paragraph Instance 1 |
      | field_simple_paragraph_desc  | This is our first paragraph  |
    When I go to "/admin/reports/paragraphs"
    Then I should see "Example Paragraph Instance 1"
     And I should see "This is our first paragraph"

  Scenario: I can create paragraphs using a new-style table.
    Given "simple_paragraph" paragraphs:
			| paragraph_name      | field_simple_paragraph_title  | field_simple_paragraph_desc  |
			| example_paragraph_2 | Example Paragraph Instance 2  | This is our second paragraph |
			| example_paragraph_3 | Example Paragraph Instance 3  | This is our third paragraph  |
    When I go to "/admin/reports/paragraphs"
    Then I should see "Example Paragraph Instance 2"
     And I should see "This is our second paragraph"
    Then I should see "Example Paragraph Instance 3"
     And I should see "This is our third paragraph"

  Scenario: I can create paragraphs from a file.
    Given I run "ls features/entities/paragraphs-simple.table.txt"
     Then I should get:
     """
     features/entities/paragraphs-simple.table.txt
     """
    Given "simple_paragraph" paragraphs from "features/entities/paragraphs-simple.table.txt"
    Given I am logged in as an "Administrator"
     When I go to "/admin/reports/paragraphs"
     Then I should see "Example Paragraph Instance 4"
      And I should see "This is our fourth paragraph"
      And I should see "Example Paragraph Instance 5"
      And I should see "This is our fifth paragraph"

  Scenario: After the scenario creating paragraphs finishes, the paragraphs don't exist anymore.
    Given I am logged in as a "Administrator"
     When I go to "/admin/reports/paragraphs"
    Then I should not see "Example Paragraph Instance 1"
     And I should not see "This is our first paragraph"
     Then I should not see "Example Paragraph Instance 2"
      And I should not see "This is our second paragraph"
      And I should not see "Example Paragraph Instance 3"
      And I should not see "This is our third paragraph"
      And I should not see "Example Paragraph Instance 4"
      And I should not see "This is our fourth paragraph"
      And I should not see "Example Paragraph Instance 5"
      And I should not see "This is our fifth paragraph"
