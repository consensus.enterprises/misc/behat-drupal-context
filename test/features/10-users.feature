@admin @api @users @from-file
Feature: Test creating users from file.
  In order to easily maintain collections of "standard" users in table files
  As a Developer
  I need a Behat step to create users from a markdown text table stored in a file.

  Scenario: Create some example users and check that they are created as expected.
    Given I run "ls features/entities/users.table.txt"
     Then I should get:
     """
     features/entities/users.table.txt
     """
    Given users from "features/entities/users.table.txt"
    Given I am logged in as a "Administrator"
     When I go to "/admin/people"
     Then I should see "user101"
      And I should see "user201"
      And I should see "user301"

  Scenario: After the scenario creating users finishes, the users don't exist anymore.
    Given I am logged in as a "Administrator"
     When I go to "/admin/people"
     Then I should not see "user101"
      And I should not see "user201"
      And I should not see "user301"
