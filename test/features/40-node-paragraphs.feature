@admin @api @paragraphs @nodes
Feature: Test creating Nodes with attached  Paragraph entities.
  In order to test Drupal applications with Paragraphs
  As a Developer
  I need a Behat step to create nodes with paragraphs attached.

  Scenario: Confirm a basic content type with a paragraph field exists
    Given I am logged in as an "Administrator"
     When I go to "admin/structure/types"
     Then I should see "Simple Paragraph Nodetype"
      And I should see "Simple content type to make use of a simple paragraph."
     When I go to "admin/structure/types/manage/simple_paragraph_nodetype/fields"
     Then I should see "Simple Paragraph"
      And I should see "field_simple_node_paragraph"

  Scenario: I can create nodes with paragraphs.
    Given I am logged in as an "Administrator"
    Given users from "features/entities/users.table.txt"
    Given a "simple_paragraph" paragraph named "example_paragraph_1":
      | field_simple_paragraph_title | Example Paragraph Instance 1 |
      | field_simple_paragraph_desc  | This is our first paragraph  |
      And "simple_paragraph" paragraphs:
      | paragraph_name      | field_simple_paragraph_title  | field_simple_paragraph_desc  |
      | example_paragraph_4 | Example Paragraph Instance 4  | This is our fourth paragraph |
      And "simple_paragraph" paragraphs from "features/entities/paragraphs-simple.table.txt"

    Given "simple_paragraph_nodetype" content:
      | title              | status | created            | author  | field_simple_node_paragraph |
      | Simple Paragraph 1 | 1      | 2023-02-02 09:00am | user101 | example_paragraph_1         |
      | Simple Paragraph 2 | 1      | 2023-02-02 09:00am | user101 | example_paragraph_4         |
      And "simple_paragraph_nodetype" content from "features/entities/node-paragraphs.table.txt"
     When I go to "admin/content"
     Then I should see "Simple Paragraph 1"
     When I click "Simple Paragraph 1"
     Then I should see "Simple Paragraph 1" in the "Page Title" region
      And I should see "Example Paragraph Instance 1"
      And I should see "This is our first paragraph"
     When I go to "admin/content"
     Then I should see "Simple Paragraph 2"
     When I click "Simple Paragraph 2"
      And I should see "Example Paragraph Instance 4"
      And I should see "This is our fourth paragraph"
     When I go to "admin/content"
     Then I should see "Simple Paragraph 3"
     When I click "Simple Paragraph 3"
      And I should see "Example Paragraph Instance 5"
      And I should see "This is our fifth paragraph"

  Scenario: After the scenario creating paragraphs finishes, the paragraphs don't exist anymore.
    Given I am logged in as a "Administrator"
     When I go to "/admin/content"
     Then I should not see "Simple Paragraph 1"
