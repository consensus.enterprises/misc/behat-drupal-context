@admin @api
Feature: Administrator role.
  In order to manage site configuration
  As an Administrator
  I need to access administrative functions.

  Scenario: Login as user with the "Administrator" role.
    Given I am not logged in
     When I go to "/user/login"
     When I am logged in as a "administrator"
