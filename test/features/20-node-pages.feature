@admin @api @nodes @node-page @from-file
Feature: Test creating Basic Page nodes from file.
  In order to easily maintain collections of "standard" content nodes in table files
  As a Developer
  I need a Behat step to create nodes from a markdown text table stored in a file.

  Scenario: Create some example nodes and check that they are created as expected.
    Given I run "ls features/entities/node-pages.table.txt"
     Then I should get:
     """
     features/entities/node-pages.table.txt
     """
    Given users from "features/entities/users.table.txt"
    Given "page" content from "features/entities/node-pages.table.txt"
      And "page" content:
      | title                | status | created            | author  | body                   |
      | Example Basic Page 1 | 1      | 2023-02-02 09:00am | user101 | This is the FIRST BODY |
    Given I am logged in as a "Administrator"
     When I go to "/admin/content"
     Then I should see "Example Basic Page 1"
      And I should see "Example Basic Page 2"
     When I click "Example Basic Page 2"
     Then I should see "This is the SECOND BODY"

  Scenario: After the scenario creating users finishes, the users don't exist anymore.
    Given I am logged in as a "Administrator"
     When I go to "/admin/content"
     Then I should not see "Example Basic Page 1"
      And I should not see "Example Basic Page 2"
