<?php

use Behat\Behat\Context\Context;
use Behat\Gherkin\Node\PyStringNode;
use Behat\Gherkin\Node\TableNode;

/**
 * Defines application features from the specific context.
 */
class FeatureContext implements Context {

  /**
   * @Given I wait :seconds seconds
   */
  public function iWait($seconds) {
    sleep($seconds);
  }
}
