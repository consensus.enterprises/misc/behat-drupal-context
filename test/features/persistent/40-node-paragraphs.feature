@admin @api @paragraphs @nodes @persistent
Feature: Test creating persistent Nodes with attached  Paragraph entities.
  In order to demonstrate Drupal applications with Paragraphs
  As a Developer
  I need a Behat step to create persistent nodes with paragraphs attached.

  Scenario: Confirm a basic content type with a paragraph field exists
    Given I am logged in as an "Administrator"
     When I go to "admin/structure/types"
     Then I should see "Simple Paragraph Nodetype"
      And I should see "Simple content type to make use of a simple paragraph."
     When I go to "admin/structure/types/manage/simple_paragraph_nodetype/fields"
     Then I should see "Simple Paragraph"
      And I should see "field_simple_node_paragraph"

  Scenario: I can create nodes with paragraphs.
    Given I am logged in as an "Administrator"
    Given persistent users from "features/entities/users.table.txt"
    Given a "simple_paragraph" persistent paragraph named "example_paragraph_1":
      | field_simple_paragraph_title | Example Paragraph Instance 1 |
      | field_simple_paragraph_desc  | This is our first paragraph  |
     And persistent "simple_paragraph" paragraphs:
      | paragraph_name      | field_simple_paragraph_title  | field_simple_paragraph_desc  |
      | example_paragraph_2 | Example Paragraph Instance 2  | This is our second paragraph |
      | example_paragraph_3 | Example Paragraph Instance 3  | This is our third paragraph  |
     And persistent "simple_paragraph" paragraphs from "features/entities/paragraphs-simple.table.txt"
    Given persistent "simple_paragraph_nodetype" content:
      | title              | status | created            | author  | field_simple_node_paragraph |
      | Simple Paragraph 1 | 1      | 2023-02-02 09:00am | user101 | example_paragraph_1         |
      | Simple Paragraph 2 | 1      | 2023-02-02 09:00am | user102 | example_paragraph_2         |
      | Simple Paragraph 3 | 1      | 2023-02-02 09:00am | user103 | example_paragraph_3         |
      | Simple Paragraph 4 | 1      | 2023-02-02 09:00am | user103 | example_paragraph_4         |
      | Simple Paragraph 5 | 1      | 2023-02-02 09:00am | user103 | example_paragraph_5         |
     When I go to "admin/content"
     Then I should see "Simple Paragraph 1"
      And I should see "Simple Paragraph 2"
      And I should see "Simple Paragraph 3"
      And I should see "Simple Paragraph 4"
      And I should see "Simple Paragraph 5"
     When I click "Simple Paragraph 1"
     Then I should see "Simple Paragraph 1" in the "Page Title" region
      And I should see "Example Paragraph Instance 1"
      And I should see "This is our first paragraph"

  Scenario: After the scenario creating paragraphs finishes, the paragraphs don't exist anymore.
    Given I am logged in as a "Administrator"
     When I go to "/admin/content"
     Then I should see "Simple Paragraph 1"

  Scenario: Clean up persistent paragraphs.
    Given I am logged in as an "Administrator"
      And I delete "simple_paragraph" paragraph named "example_paragraph_1":
      | field_simple_paragraph_title | Example Paragraph Instance 1 |
      | field_simple_paragraph_desc  | This is our first paragraph  |
      And I delete "simple_paragraph_nodetype" content:
      | title              | status | created            | author  | field_simple_node_paragraph |
      | Simple Paragraph 1 | 1      | 2023-02-02 09:00am | user101 | example_paragraph_1         |
      | Simple Paragraph 2 | 1      | 2023-02-02 09:00am | user102 | example_paragraph_2         |
      | Simple Paragraph 3 | 1      | 2023-02-02 09:00am | user103 | example_paragraph_3         |
      | Simple Paragraph 4 | 1      | 2023-02-02 09:00am | user103 | example_paragraph_4         |
      | Simple Paragraph 5 | 1      | 2023-02-02 09:00am | user103 | example_paragraph_5         |
      And I delete "simple_paragraph" paragraphs:
      | paragraph_name      | field_simple_paragraph_title  | field_simple_paragraph_desc  |
      | example_paragraph_2 | Example Paragraph Instance 2  | This is our second paragraph |
      | example_paragraph_3 | Example Paragraph Instance 3  | This is our third paragraph  |
      And I delete "simple_paragraph" paragraphs from "features/entities/paragraphs-simple.table.txt"
      And I delete users from "features/entities/users.table.txt"
    When I go to "/admin/content"
    When I go to "/admin/reports/paragraphs"
    Then I should not see "Example Paragraph Instance 1"
