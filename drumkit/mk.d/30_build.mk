# Build (and clean up) a Drupal codebase.

.PHONY: clean-build build

check-composer-cache:
	@if [ -z ${COMPOSER_CACHE_DIR} ]; then echo -e "$(YELLOW)Missing required variable $(GREY)COMPOSER_CACHE_DIR$(YELLOW).$(RESET)"; echo -e "$(BOLD)$(WHITE)Remember to bootstrap Drumkit ($(GREEN). d$(WHITE))$(RESET)"; exit 1; fi

build: #check-composer-cache composer ## Build Composer codebase.
	@$(MAKE-QUIET) build-real
build-real:
	@$(ECHO) "$(YELLOW)Beginning build of codebase. (Be patient. This may take a while.)$(RESET)"
	cd test && $(COMPOSER) install --no-progress $(QUIET)
	@$(ECHO) "$(YELLOW)Completed build of codebase.$(RESET)"

clean-composer-cache: ## Clean Composer cache.
ifneq ("$(wildcard $(COMPOSER_CACHE_DIR))","")
	@rm -rf $(COMPOSER_CACHE_DIR)
	@mkdir -p $(COMPOSER_CACHE_DIR)
	$(ECHO) "$(YELLOW)Deleted Composer cache contents.$(RESET)"
endif

clean-build: ## Clean Composer built code.
	@$(ECHO) "$(YELLOW)Beginning clean of composer-built code.$(RESET)"
	@chmod -R +w test/web/
	@if [ -d vendor/ ] ; then rm -r vendor ; fi
	@if [ -d web/core/ ] ; then rm -r web/core/ ; fi
	@if [ -d web/libraries/ ] ; then rm -r web/libraries/ ; fi
	@if [ -d web/modules/contrib/ ] ; then rm -r web/modules/contrib/ ; fi
	@if [ -d web/profiles/contrib/ ] ; then rm -r web/profiles/contrib/ ; fi
	@if [ -d web/themes/contrib/ ] ; then rm -r web/themes/contrib/ ; fi
	@$(ECHO) "$(YELLOW)Completed clean of composer-built code.$(RESET)"

update: ## Run composer update
	@$(MAKE-QUIET) update-real
update-real:
	@$(ECHO) "$(YELLOW)Beginning update of codebase. (Be patient. This may take a while.)$(RESET)"
	$(COMPOSER) update --no-progress $(QUIET)
	@$(ECHO) "$(YELLOW)Completed update of codebase.$(RESET)"
