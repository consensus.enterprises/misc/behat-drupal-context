tests:
	cd test && $(BEHAT)

tests-ci:
	cd test && $(BEHAT) --profile=ci --stop-on-failure

full-tests: install tests
