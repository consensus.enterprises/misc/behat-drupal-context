<?php
namespace Consensus\BehatDrupalContext\Util;

use Behat\Gherkin\Node\TableNode;
use Exception;

trait TableNodeFromFileTrait {

  /**
   * @param string $file The path to a .table.txt (markdown table) file containing field content.
   * @return \Behat\Gherkin\Node\TableNode
   */
  public function readTableNodeFromFile($file) {
    $base_dir = getenv('PWD');
    printf("readTableNodeFromFile(%s): changing to base directory: %s\n", $file, $base_dir);
    chdir($base_dir);

    $lines = file($file);

    $table_array = array();
    foreach ($lines as $line) {
      try {
        $line_array = array_filter(str_getcsv($line, "|"));
        array_walk($line_array, function (&$value, $key) { $value = trim($value); });
        $table_array[] = $line_array;
      } catch (Exception $e) {
        print("Exception parsing .table.txt file at line $line: ".$e->getMessage()."\n");
      }
    }

    return new TableNode($table_array);
  }
}
