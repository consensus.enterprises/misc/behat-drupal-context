<?php

namespace Consensus\BehatDrupalContext\Manager;

use Drupal\DrupalExtension\Manager\DrupalUserManager;

/**
 * Custom implementation of the Drupal user manager service, which
 * is used to override the standard DrupalUserManager's clearUsers()
 * method, to ensure that users created in a persistent scenario
 * do not get cleaned up after the scenario ends.
 */
class ConsensusUserManager extends DrupalUserManager {

  /**
   * {@inheritdoc}
   */
  public function clearUsers() { return; }
}
