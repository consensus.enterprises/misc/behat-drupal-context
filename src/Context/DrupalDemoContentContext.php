<?php
namespace Consensus\BehatDrupalContext\Context;

use Consensus\BehatDrupalContext\Context\Traits\Steps\PersistentUsersStepsTrait;
use Consensus\BehatDrupalContext\Context\Traits\Steps\PersistentNodesStepsTrait;
use Consensus\BehatDrupalContext\Context\Traits\Steps\PersistentParagraphsStepsTrait;
use Consensus\BehatDrupalContext\Context\Traits\Steps\PersistentEntityStepsTrait;

/**
 * Defines steps to create fixtures for development.
 */
class DrupalDemoContentContext extends DrupalContentContextBase {

  # Pull in step definitions for persistent users.
  use PersistentUsersStepsTrait;
  use PersistentNodesStepsTrait;
  use PersistentParagraphsStepsTrait;
  use PersistentEntityStepsTrait;

}
