<?php
namespace Consensus\BehatDrupalContext\Context\Traits\Steps;

use Behat\Gherkin\Node\TableNode;
use Consensus\BehatDrupalContext\Context\Traits\ParagraphsBaseTrait;

trait PersistentParagraphsStepsTrait {

  use ParagraphsBaseTrait;

  /**
   * The cleanup routine in this variant does nothing, so that paragraphs
   * don't get cleaned up.
   *
   * @AfterScenario
   */
  public function cleanup() { return; }

  /**
   * Creates persistent paragraph of the given type, provided in the form:
   * | title     | My node        |
   * | Field One | My field value |
   * | author    | Joe Editor     |
   * | status    | 1              |
   * | ...       | ...            |
   *
   * @Given a/an :type persistent paragraph named :name:
   */
  public function createPersistentParagraphs($type, $name, TableNode $table) {
    $this->_createParagraph($type, $name, $table);
	}

  /**
   * Creates paragraph of the given type, provided in the form:
   * | paragraph_name    | field_simple_paragraph_title | field_simple_paragraph_desc |
   * | example_paragraph | Title goes here              | Decription goes here        |
   *
   * @Given persistent :type paragraphs:
   */
  public function createPersistentTableParagraphs($type, TableNode $table) {
    $this->_createTableParagraphs($type, $table);
  }

  /**
   * Creates paragraph of the given type, provided from a file in the form:
   * | paragraph_name    | field_simple_paragraph_title | field_simple_paragraph_desc |
   * | example_paragraph | Title goes here              | Decription goes here        |
   *
   * @Given persistent :type paragraphs from :file
   */
  public function createParagraphsFrom($type, $file) {
    $this->_createParagraphsFrom($type, $file);
  }

}
