<?php

namespace Consensus\BehatDrupalContext\Context\Traits\Steps;

/**
 * Behat Context for adding paragraphs with API calls
 */
use Behat\Behat\Context\Context;
use Behat\Gherkin\Node\TableNode;
use Drupal\DrupalExtension\Hook\Scope\EntityScope;

trait ParagraphsFromFileStepsTrait {

  use ParagraphsStepsTrait;

  /**
   * Creates paragraph of the given type, provided from a file in the form:
   * | paragraph_name    | field_simple_paragraph_title | field_simple_paragraph_desc |
   * | example_paragraph | Title goes here              | Decription goes here        |
   *
   * @Given :type paragraphs from :file
   */
  public function createParagraphsFrom($type, $file) {
    $this->_createParagraphsFrom($type, $file);
  }

  /**
   * @Given I delete :type paragraphs from :file
   */
  public function iDeleteParagraphsFrom($type, $file) {
    $paraTable = $this->readTableNodeFromFile($file);
    $this->iDeleteParagraphsTable($type, $paraTable);
  }

}
