<?php

namespace Consensus\BehatDrupalContext\Context\Traits\Steps;

trait UsersFromFileStepsTrait {

  /**
   * Create a set of users from a table in a file.
   *
   * Provide user data in a file containing a table in the following format:
   *  | uid | name          | password | email                     | roles            |
   *  | 100 | test_location | pwd      | test_location@example.com | location_manager |
   *  | 200 | test_shipment | pwd      | test_shipment@example.com | shipment_manager |
   *
   * @Given users from :file
   */
  public function createUsersFrom($file) {
    $usersTable = $this->readTableNodeFromFile($file);
    $this->drupalContext->createUsers($usersTable);
  }

}
