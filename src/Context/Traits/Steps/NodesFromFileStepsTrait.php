<?php
namespace Consensus\BehatDrupalContext\Context\Traits\Steps;

trait NodesFromFileStepsTrait {

  /**
   * Create a standard set of nodes from a table in a file.
   *
   * Provide node data in a file containing a table in the following format:
   * | title    | author     | status | created           |
   * | My title | Joe Editor | 1      | 2014-10-17 8:00am |
   * | ...      | ...        | ...    | ...               |
   *
   * @Given :type content from :file
   */
  public function createNodesFrom($type, $file) {
    $nodesTable = $this->readTableNodeFromFile($file);
    $this->drupalContext->createNodes($type, $nodesTable);
  }

}
