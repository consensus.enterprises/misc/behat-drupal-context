<?php

namespace Consensus\BehatDrupalContext\Context\Traits\Steps;

use Consensus\BehatDrupalContext\Context\Traits\ParagraphsBaseTrait;

/**
 * Behat Context for adding paragraphs with API calls
 */
use Behat\Behat\Context\Context;
use Behat\Gherkin\Node\TableNode;
use Drupal\DrupalExtension\Hook\Scope\EntityScope;

trait ParagraphsStepsTrait {

  use ParagraphsBaseTrait;

  /**
   * Remove any created products.
   *
   * @AfterScenario
   */
  public function cleanup() {
    // Remove any paragraphs that were created.
    foreach ($this->paragraphEntities as $entity) {
      $this->drupalContext->getDriver()->entityDelete($this->paragraphEntityID, $entity);
    }
    $this->paragraphEntities = [];
    $this->paragraphNames = [];
  }

  /**
   * Creates paragraph of the given type, provided in the form:
   * | title     | My node        |
   * | Field One | My field value |
   * | author    | Joe Editor     |
   * | status    | 1              |
   * | ...       | ...            |
   *
   * @Given a/an :type paragraph named :name:
   */
  public function createParagraph($type, $name, TableNode $fields) {
    $this->_createParagraph($type, $name, $fields);
  }

  /**
   * Creates paragraph of the given type, provided in the form:
   * | paragraph_name    | field_simple_paragraph_title | field_simple_paragraph_desc |
   * | example_paragraph | Title goes here              | Decription goes here        |
   *
   * @Given :type paragraphs:
   */
  public function createTableParagraphs($type, TableNode $table) {
    $this->_createTableParagraphs($type, $table);
  }

  /**
   * @Given I delete :type paragraph named :name:
   */
  public function iDeleteParagraph($type, $name, TableNode $table) {
    $entity = [
      'type' => $type,
    ];
    foreach ($table->getRowsHash() as $field => $value) {
      $entity[$field] = $value;
    }

    $this->paragraphDelete($entity);
  }

  /**
   * @Given I delete :type paragraphs:
   */
  public function iDeleteParagraphsTable($type, TableNode $table) {
    foreach ($table->getHash() as $row) {

      # Initialize paragraph name and an empty entity.
      $name = array_shift($row);
      $entity = [
        'type' => $type,
      ];

      # Loop over remainder of row to populate entity fields.
      foreach ($row as $field => $value) {
        $entity[$field] = $value;
      }

      # Instantiate the paragraph entity.
      $this->paragraphDelete($entity);
    }
  }

  /**
   * Set up paragraph entity references for nodes.
   * @TODO: Consider dropping this hook used for non-persistent nodes with
   * paragraphs, and call preprocessEntityReferences directly as we do for
   * persistent variants.
   *
   * @beforeNodeCreate
   */
  public function beforeNodeCreateHook(EntityScope $scope) {
    // This is missing in the Drupal driver
    $this->preprocessEntityReferenceFieldsForParagraphs('node', $scope->getEntity());
  }

  /**
   * Set up paragraph entity references for users.
   *
   * @beforeUserCreate
   */
  public function beforeUserCreateHook(EntityScope $scope) {
    // This is missing in the Drupal driver
    $this->preprocessEntityReferenceFieldsForParagraphs('user', $scope->getEntity());
  }

}
