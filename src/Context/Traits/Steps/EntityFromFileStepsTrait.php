<?php
namespace Consensus\BehatDrupalContext\Context\Traits\Steps;

trait EntityFromFileStepsTrait {
  use EntityStepsTrait;

  /**
   * @Given content in the :bundle bundle of the :type entity from :file
   * @Given :type entities from the :bundle bundle from :file
   *
   * Provide entities data in a file containing a table in the following format:
   * | field_eck_example_field1 | field_eck_example_field2    |
   * | this is some text        | this is another field value |
   * | ...      | ...        |
   */
  public function contentInTheBundleOfTheEntityFrom($bundle, $type, $file) {
    $table = $this->readTableNodeFromFile($file);
    $this->_createEntities($bundle, $type, $table);
  }

}
