<?php
namespace Consensus\BehatDrupalContext\Context\Traits\Steps;

use Behat\Gherkin\Node\TableNode;
use Consensus\BehatDrupalContext\Context\Traits\NodesBaseTrait;

trait PersistentNodesStepsTrait {

  use NodesBaseTrait;

  /**
   * @Given persistent :type content:
   */
  public function createPersistentNodes($type, TableNode $nodesTable) {
    foreach ($nodesTable->getHash() as $nodeHash) {
      $this->createNode($type, $nodeHash);
    }
  }

  /**
   * @Given persistent :type content from :file
   */
  public function persistentContentFrom($type, $file) {
    $table = $this->readTableNodeFromFile($file);
    $this->createPersistentNodes($type, $table);
  }

  /**
   * @Given I delete :type content:
   */
  public function iDeleteContent($type, TableNode $nodesTable) {
    foreach ($nodesTable->getHash() as $nodeHash) {
      $this->nodeDelete($nodeHash);
    }
  }

  /**
   * @Given I delete :type content from :file
   */
  public function iDeleteContentFrom($type, $file) {
    $table = $this->readTableNodeFromFile($file);
    $this->iDeleteContent($type, $table);
  }

}
