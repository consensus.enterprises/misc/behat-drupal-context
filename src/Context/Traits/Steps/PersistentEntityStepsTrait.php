<?php
namespace Consensus\BehatDrupalContext\Context\Traits\Steps;

use Behat\Gherkin\Node\TableNode;

use Consensus\BehatDrupalContext\Context\Traits\EntityBaseTrait;

trait PersistentEntityStepsTrait {

  use EntityBaseTrait;

  /**
   * The cleanup routine in this variant does nothing, so that entity
   * don't get cleaned up.
   *
   * @AfterScenario
   */
  public function entity_cleanup() { return; }

  /**
   * Creates persistent entity of the given type, provided in the form:
   * |  title     | field_eck_example_field1 | field_eck_example_field2    |
   * | abc title  | this is some text        | this is another field value |
   * |   ...      |           ...            |           ...               |
   *
   * @Given persistent content in the :bundle bundle of the :type entity:
   * @Given persistent :type entities of the :bundle bundle:
   */
  public function persistentContentEntities($bundle, $type, TableNode $table) {
    $this->_createEntities($bundle, $type, $table);
  }

  /**
   * @Given persistent content in the :bundle bundle of the :type entity from :file
   * @Given persistent :type entities of the :bundle bundle from :file
   */
  public function persistentContentEntitiesFrom($bundle, $type, $file) {
    $table = $this->readTableNodeFromFile($file);
    $this->_createEntities($bundle, $type, $table);
  }

  /**
   * @Given I delete persistent content in the :bundle bundle of the :type entity:
   * @Given I delete persistent :type entities of the :bundle bundle:
   */
  public function deletePersistentContentEntities($bundle, $type, TableNode $table) {
    $this->_deleteEntities($bundle, $type, $table);
  }

  /**
   * @Given I delete persistent content in the :bundle bundle of the :type entity from :file
   */
  public function deletePersistentContentEntitiesFrom($bundle, $type, $file) {
    $table = $this->readTableNodeFromFile($file);
    $this->_deleteEntities($bundle, $type, $table);
  }

}
