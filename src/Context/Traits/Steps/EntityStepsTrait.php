<?php
namespace Consensus\BehatDrupalContext\Context\Traits\Steps;
use Behat\Gherkin\Node\TableNode;

use Consensus\BehatDrupalContext\Context\Traits\EntityBaseTrait;

trait EntityStepsTrait {
  use EntityBaseTrait;

  /**
   * Create a standard set of entities from a table in a file.
   *
   * Provide entities data in a file containing a table in the following format:
   * | field_eck_example_field1 | field_eck_example_field2    |
   * | this is some text        | this is another field value |
   * | ...      | ...        |
   *
   * @Given content in the :bundle bundle of the :type entity:
   * @Given :type entities of the :bundle bundle:
   */
  public function contentInTheBundleOfTheEntity($bundle, $type, TableNode $table) {
#    print_r(sprintf("contentInTheBundleOfTheEntity[%s:%s]\n", $type, $bundle));
    $this->_createEntities($bundle, $type, $table);
  }

  #@TODO: We should probably have a "Delete" step in the non-persistent case as well.

}
