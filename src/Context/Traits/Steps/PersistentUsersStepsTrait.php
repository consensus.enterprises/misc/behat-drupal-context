<?php
namespace Consensus\BehatDrupalContext\Context\Traits\Steps;

use Behat\Gherkin\Node\TableNode;
use Consensus\BehatDrupalContext\Context\Traits\UsersBaseTrait;

trait PersistentUsersStepsTrait {

  use UsersBaseTrait;

  /**
   * Creates fixture users.
   *
   * Provide user data in the following format:
   *  | uid | name          | password | email                     | roles            |
   *  | 100 | test_location | pwd      | test_location@example.com | location_manager |
   *  | 200 | test_shipment | pwd      | test_shipment@example.com | shipment_manager |
   *
   * N.B. See DrupalContext:createUsers(). We're not overriding the method
   * here because we still want the original behaviour (ie. deleting test
   * users) for testing purposes.
   *
   * @Given persistent users:
   */
  public function createPersistentUsers(TableNode $usersTable) {
    foreach ($usersTable->getHash() as $userHash) {
      $this->createUser($userHash);
    }
  }

  /**
   * @Given persistent users from :file
   *
   * Provide user data in the following format:
   *  | uid | name          | password | email                     | roles            |
   *  | 100 | test_location | pwd      | test_location@example.com | location_manager |
   *  | 200 | test_shipment | pwd      | test_shipment@example.com | shipment_manager |
   *
   * N.B. See DrupalContext:createUsers(). We're not overriding the method
   * here because we still want the original behaviour (ie. deleting test
   * users) for testing purposes.
   */
  public function persistentUsersFrom($file) {
    $table = $this->readTableNodeFromFile($file);
    $this->createPersistentUsers($table);
	}

  /**
   * @Given I delete users:
   */
  public function deleteUsers(TableNode $usersTable) {
    foreach ($usersTable->getHash() as $userHash) {
      $this->userDelete($userHash);
    }
  }

  /**
   * @Given I delete users from :file
   */
  public function deleteUsersFrom($file) {
    $table = $this->readTableNodeFromFile($file);
    $this->deleteUsers($table);
  }
}
