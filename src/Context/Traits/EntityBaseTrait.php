<?php

namespace Consensus\BehatDrupalContext\Context\Traits;

use Behat\Gherkin\Node\TableNode;
use Drupal\node\Entity;
use Drupal\Core\Entity\EntityTypeManagerInterface;

trait EntityBaseTrait {

   protected $genericEntities = [];

  /**
   * Create a set of entities from table.
   */
  protected function _createEntities(string $bundle, string $type, TableNode $table) {
    foreach ($table->getHash() as $row) {
        $entity = [
          'type' => $bundle,
        ];

        # Loop over each cell in the the row to populate entity fields.
        foreach ($row as $field => $value) {
          $entity[$field] = $value;
        }

        if (!array_key_exists('title', $entity)) {
          throw new \Exception("The table of entities to create must have a 'title' column.");
        }

        $existing_entity = $this->_loadEntity($type, $bundle, $entity['title']);
        if ($existing_entity) {
          printf("Skipping creation of existing %s %s (title: %s)\n", $bundle, $type, $entity['title']);
          continue;
        }

        $this->preprocessEntityReferenceFieldsForEntities($type, $entity);
        $drupal_driver = $this->drupalContext->getDriver();
        // Clear the Drupal cache to ensure that we're picking up newly defined
        // bundles, etc. We need to ensure that we're in the Drupal root for
        // the cache to be rebuilt properly.
        $cwd = getcwd();
        chdir($this->getDrupalRoot());
        $drupal_driver->clearCache();
        // Change back to the original directory to avoid side effects of the change
        chdir($cwd);
        $entityId = $drupal_driver->createEntity($type, (object) $entity);
        $this->genericEntities[$type][$entity['title']] = $entityId;
    }
  }

  /**
   * Return the proper Drupal root, depending on the current working directory.
   */
  protected function getDrupalRoot(): string {
      // @TODO: Maybe we can pull this directly from Drupal, or something. This feels fragile.
      $cwd = getcwd();
      $drupal_root = $this->getDrupalParameter('drupal')['drupal_root'];
      if (basename($cwd) == $drupal_root) {
          return $cwd;
      }
      return $cwd . DIRECTORY_SEPARATOR . $drupal_root;
  }

  /**
   * Create a set of entities from table.
   */
  protected function _deleteEntities($bundle, $type, $table) {
    printf("_deleteEntities(bundle=%s, $type=%s)\n", $bundle, $type);

    foreach ($table->getHash() as $row) {
      $entity = [
        'type' => $bundle
      ];

      # Loop over remainder of row to populate entity fields.
      foreach ($row as $field => $value) {
        $entity[$field] = $value;
      }
      $this->_deleteEntity($type, $entity);
    }
  }

  /**
   * Delete an entity based on its entityHash data.
   *
   * @param string $type The entity type to delete.
   * @param array $entityHash The hashed array of node data.
   */
  protected function _deleteEntity($type, $entityHash) {
    $entity = $this->_loadEntity($type, $entityHash['type'], $entityHash['title']);

    #@TODO: This if is untested, and probably won't work if a entity doesn't exist.
    if (!$entity->id()) {
      printf("Skipping deletion of non-existent entity [%s]\n", $entityHash['title']);
    }
    $entity->delete();
  }

  /**
   * Load a entity based on field properties.
   *
   * @param string $type The entity type.
   * @param string $bundle The entity bundle.
   * @param string $title The title of the entity.
   *
   * @return object The entity, if found.
   */
  protected function _loadEntity(string $type, string $bundle, string $title) {
    $properties = [
      'type' => $bundle, # Really Drupal? Really?
      'title' => $title,
    ];
    $entity_array = \Drupal::entityTypeManager()
      ->getStorage($type)
      ->loadByProperties($properties);

    #@TODO: This array_pop means that entityDelete above will only see the
    # first entity found, and thus not delete others with the same properties
    # that happen to be present. We /may/ want to alter this behaviour.
    return array_pop($entity_array);
  }

  /**
   * Detect entity references and create an entity
   *
   * @param string $entity_type_id
   * @param array $entity_array
   *
   * @throws \Exception
   */
  protected function preprocessEntityReferenceFieldsForEntities(string $entity_type_id, array &$entity_array) {
    /** @var \Drupal\field\Entity\FieldStorageConfig[] $fields */
    $fields = \Drupal::service('entity_field.manager')
      ->getFieldStorageDefinitions($entity_type_id);
    foreach ((array)$entity_array as $field_name => $value) {
      if (is_array($value) || !isset($fields[$field_name])
        || !$fields[$field_name]->getSettings()
        || !isset($fields[$field_name]->getSettings()['target_type'])
        || $fields[$field_name]->getSettings()['target_type'] !== $entity_type_id) {
        continue;
      }
      $target_ids = [];
      $revision_ids = [];
      $values = (strpos($value, ',') !== FALSE)
        ? array_map('trim', explode(',', $value))
        : [$value];
      foreach ($values as $value) {
        $target_id = isset($this->genericEntities[$entity_type_id][$value]) ? $this->genericEntities[$entity_type_id][$value] : NULL;
        if (!$target_id) {
          throw new \Exception(sprintf('Referenced entity name "%s" not found.', $value));
        }
        $referencedEntity = \Drupal::entityTypeManager()->getStorage($entity_type_id)->load($target_id);
        $target_ids[] = $target_id;
        $revision_ids[] = $referencedEntity->getRevisionId();
      }
      if (empty($target_ids) || empty($revision_ids)) {
        continue;
      }
      $column_name = "$field_name:target_id";
      $entity_array[$column_name] = implode(',', $target_ids);
      $column_name = "$field_name:target_revision_id";
      $entity_array[$column_name] = implode(',', $revision_ids);
    }
  }

  /**
   * Remove any created entities.
   *
   * @AfterScenario
   */
  public function entity_cleanup() {
    foreach($this->genericEntities as $entityType => $entities) {
        foreach($entities as $entityTitle => $entityId) {
          $entity = \Drupal::entityTypeManager()->getStorage($entityType)->load($entityId->id);
          $entity->delete();
        }
    }
  }

}



