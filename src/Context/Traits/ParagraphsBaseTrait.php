<?php

namespace Consensus\BehatDrupalContext\Context\Traits;

/**
 * Behat Context for adding paragraphs with API calls
 */
use Behat\Behat\Context\Context;
use Behat\Gherkin\Node\TableNode;
use Drupal\DrupalExtension\Hook\Scope\EntityScope;

trait ParagraphsBaseTrait {

  protected $paragraphEntities = [];
  protected $paragraphEntityID = 'paragraph';
  protected $paragraphNames = [];

  /**
   * Create a set of paragraphs
   */
  protected function _createParagraph($type, $name, TableNode $table) {
    $entity = (object) [
      'type' => $type,
    ];
    foreach ($table->getRowsHash() as $field => $value) {
      $entity->{$field} = $value;
    }

    $this->rawCreateParagraph($name, $entity);
  }

  protected function _createTableParagraphs($type, TableNode $table) {
    foreach ($table->getHash() as $row) {

      # Initialize paragraph name and an empty entity.
      $name = array_shift($row);
      $entity = (object) [
        'type' => $type,
      ];

      # Loop over remainder of row to populate entity fields.
      foreach ($row as $field => $value) {
        $entity->{$field} = $value;
      }

      # Instantiate the paragraph entity.
      $this->rawCreateParagraph($name, $entity);
    }
	}

  protected function _createParagraphsFrom($type, $file) {
    $paraTable = $this->readTableNodeFromFile($file);
    $this->_createTableParagraphs($type, $paraTable);
  }


  /**
   * Create a paragraph.
   *
   * @param $entity
   *
   * @return saved
   *   The created paragraph.
   * @throws \Exception
   */
  protected function createEntity($entity) {
    $this->drupalContext->parseEntityFields($this->paragraphEntityID, $entity);
    $saved = $this->drupalContext->getDriver()->createEntity($this->paragraphEntityID, $entity);
    $this->paragraphEntities[] = $saved;
    return $saved;
  }

  /**
   * Create named paragraph entity and save it in the context.
   *
   * @param string $name The internal paragraph name for future reference in Scenario
   * @param \StdClass $entity The populated entity object to create
   */
  protected function rawCreateParagraph($name, $entity) {
    $this->preprocessEntityReferenceFieldsForParagraphs('paragraph', $entity);
    $saved = $this->createEntity($entity);
    $this->paragraphNames[$name] = $saved->id;
    $saved->__paragraph_name = $name;
  }

  /**
   * Detect entity references to paragraphs and resolve them before creating an entity
   *
   * @param string $entity_type_id
   * @param \StdClass $entity
   *
   * @throws \Exception
   */
  protected function preprocessEntityReferenceFieldsForParagraphs(string $entity_type_id, \StdClass $entity) {
    /** @var \Drupal\field\Entity\FieldStorageConfig[] $fields */
    $fields = \Drupal::service('entity_field.manager')
      ->getFieldStorageDefinitions($entity_type_id);
    foreach ((array)$entity as $field_name => $value) {
      if (is_array($value) || !isset($fields[$field_name])
        || !$fields[$field_name]->getSettings()
        || !isset($fields[$field_name]->getSettings()['target_type'])
        || $fields[$field_name]->getSettings()['target_type'] !== 'paragraph') {
        continue;
      }
      $target_ids = [];
      $revision_ids = [];
      $values = (strpos($value, ',') !== FALSE)
        ? array_map('trim', explode(',', $value))
        : [$value];
      foreach ($values as $value) {
        $target_id = isset($this->paragraphNames[$value]) ? $this->paragraphNames[$value] : NULL;
        if (!$target_id) {
          throw new \Exception(sprintf('Referenced paragraph name "%s" not found.', $value));
        }
        $paragraph = \Drupal\paragraphs\Entity\Paragraph::load($target_id);
        $target_ids[] = $target_id;
        $revision_ids[] = $paragraph->getRevisionId();
      }
      if (empty($target_ids) || empty($revision_ids)) {
        continue;
      }
      $column_name = "$field_name:target_id";
      $entity->$column_name = implode(',', $target_ids);
      $column_name = "$field_name:target_revision_id";
      $entity->$column_name = implode(',', $revision_ids);
    }
  }

  /**
   * Delete a node based on its nodeHash data.
   *
   * @param array $nodeHash The hashed array of node data.
   */
  protected function paragraphDelete($paragraphHash) {
    $paragraph = $this->load_paragraph_from_properties($paragraphHash);

    #@TODO: This if is untested, and probably won't work if a paragraph doesn't exist.
    if (!$paragraph->id()) {
      printf("Skipping deletion of non-existent paragraph\n");
      print_r($paragraphHash);
    }

    $paragraph->delete();
  }

  /**
   * Load a paragraph based on field properties.
   *
   * @param array $properties Hashed array of properties to look the paragraph up.
   * @return object The paragraph entity, if found.
   */
  protected function load_paragraph_from_properties($properties) {
    $entity_array = \Drupal::entityTypeManager()
      ->getStorage('paragraph')
      ->loadByProperties($properties);

    #@TODO: This array_pop means that paragraphDelete above will only see the
    # first paragraph found, and thus not delete others with the same properties
    # that happen to be present. We /may/ want to alter this behaviour.
    return array_pop($entity_array);
  }
}

