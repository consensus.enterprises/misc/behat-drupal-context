<?php

namespace Consensus\BehatDrupalContext\Context\Traits;

use Behat\Gherkin\Node\TableNode;
use Drupal\node\Entity\Node;

trait NodesBaseTrait {

  /**
   * Create a node of a particular type, given its nodeHash data array.
   *
   * @param string $type The content type to create
   * @param array $nodeHash The hashed array of field data for the node.
   */
  public function createNode($type, $nodeHash) {
    if (isset($nodeHash['nid']) && Node::load($nodeHash['nid'])) {
      print("Skipping creation of existing node (nid: {$nodeHash['nid']})\n");
      return;
    }

    $existing_node = $this->_loadEntity('node', $type, $nodeHash['title']);
    if ($existing_node) {
      printf("Skipping creation of existing %s node (title: %s)", $type, $nodeHash['title']);
      return;
    }

    $node = (object) $nodeHash;
    $node->type = $type;

    # Copied from RawDrupalContext:nodeCreate().
    #@TODO: this method will only be available if we have the ParagraphsBaseTrait.
    $this->preprocessEntityReferenceFieldsForParagraphs('node', $node);
    $this->parseEntityFields('node', $node);
    $saved = $this->getDriver()->createNode($node);
    # Here, we do NOT register the new node with the context class "registry"
    # to ensure it doesn't get cleaned up after the scenario finishes.
  }

  /**
   * Delete a node based on its nodeHash data.
   *
   * @param array $nodeHash The hashed array of node data.
   */
  protected function nodeDelete($nodeHash) {
    if (!$this->node_exists($nodeHash)) {
      printf("Skipping deletion of non-existent node[%d]: %s\n",
        isset($nodeHash['nid'])?: "n/a",
        $nodeHash['title']
      );
      return;
    }

    if (!isset($nodeHash['nid'])) {
      $nodeHash['nid'] = $this->node_id($nodeHash);
    }

    $this->getDriver()->nodeDelete((object) $nodeHash);
  }

  /**
   * Takes a hash of a node, determines whether it exists (or can be found by
   * given parameters). Attempt to look up by nid and title.
   *
   * @param array Hashed array of TableNode-style row
   * @return bool Whether the given node exists
   */
  private function node_exists($nodeHash) {
    if (isset($nodeHash['nid'])) {
      if (Node::load($nodeHash['nid'])) {
        return true;
      }
    }

    # Fall back to looking up title
    if (isset($nodeHash['title'])) {
      return $this->load_node_from_title($nodeHash['title']);
    }

    return false;
  }

  /**
   * Look up a nodes ID, in case it's not present in the nodeHash.
   *
   * @param array $nodeHash The hashed array of node data.
   */
  private function node_id($nodeHash) {
    if (isset($nodeHash['nid'])) {
      return $nodeHash['nid'];
    }

    if (isset($nodeHash['title'])) {
      $node = $this->load_node_from_title($nodeHash['title']);
      return $node->id();
    }

    return NULL;
  }

  /**
   * Look up a node by its title.
   *
   * @param string $title The node title.
   */
  private function load_node_from_title($title) {
    $node_array = \Drupal::entityTypeManager()
        ->getStorage('node')
        ->loadByProperties(['title' => $title]);
    return array_pop($node_array);
  }

}
