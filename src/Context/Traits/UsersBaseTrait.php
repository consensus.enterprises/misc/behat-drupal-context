<?php

namespace Consensus\BehatDrupalContext\Context\Traits;

use Consensus\BehatDrupalContext\Manager\ConsensusUserManager;
use Drupal\user\Entity\User;

trait UsersBaseTrait {

  /**
   * @BeforeScenario @api
   */
  public function bootstrap(): void {
    parent::bootstrap();

    // Swap in custom UserManager class that doesn't remove users.
    $this->setUserManager(new ConsensusUserManager());
  }

  /**
   * @AfterScenario
   *
   * Override the parent class method to avoid removing users creating during tests.
   */
  public function cleanUsers() { return; }

  /**
   * Custom implementation of createUser method, which creates persistent
   * users that do not get cleaned up after the Scenario finishes.
   *
   * @param array $userHash The hashed array of user data.
   */
  protected function createUser($userHash) {
    if (isset($userHash['uid'])) {
      if (User::load($userHash['uid'])) {
        printf("Skipping creation of existing user (uid: {%d})\n", $userHash['uid']);
        return;
      }
    }

    // Split out roles to process after user is created.
    $roles = [];
    if (isset($userHash['roles'])) {
      $roles = explode(',', $userHash['roles']);
      $roles = array_filter(array_map('trim', $roles));
      unset($userHash['roles']);
    }

    $user = (object) $userHash;

    # Copied from RawDrupalContext:userCreate()
    $this->dispatchHooks('BeforeUserCreateScope', $user);
    $this->parseEntityFields('user', $user);
    $this->getDriver()->userCreate($user);
    $this->dispatchHooks('AfterUserCreateScope', $user);
    # userManager is the registry of users to delete in
    # RawDrupalContext:cleanUsers() postScenario hook.
    $this->userManager->addUser($user);

    // Assign roles.
    foreach ($roles as $role) {
      $this->getDriver()->userAddRole($user, $role);
    }
  }

  /**
   * Delete a user based on its userHash data.
   *
   * @param array $userHash The hashed array of user data.
   */
  protected function userDelete($userHash) {
    if (!isset($userHash['uid']) || !User::load($userHash['uid'])) {
      printf("Skipping deletion of non-existent user (uid: {%d})\n", $userHash['uid']);
      return;
    }

    $this->getDriver()->userDelete((object) $userHash);

    # This is needed to ensure the users are actually deleted.
    $this->getDriver()->processBatch();
  }


}
