<?php
namespace Consensus\BehatDrupalContext\Context;

use Behat\Behat\Hook\Scope\BeforeScenarioScope;
use Consensus\BehatDrupalContext\Util\TableNodeFromFileTrait;
use Drupal\DrupalExtension\Context\RawDrupalContext;

/**
 * Defines steps to create fixtures for development.
 */
class DrupalContentContextBase extends \Drupal\DrupalExtension\Context\RawDrupalContext {

  use TableNodeFromFileTrait;    # Provides readTableNodeFromFile()

  /**
   * @var \Drupal\DrupalExtension\Context\DrupalContext
   */
  protected $drupalContext;

  /**
   * Bootstraps Drupal.
   *
   * We need to ensure that Drupal is properly bootstrapped before we run any
   * other hooks or execute step definitions. By calling `::getDriver()` we can
   * be sure that Drupal is ready to rock.
   *
   * This hook should be placed at the top of the first Context class listed in
   * behat.yml.
   *
   * @BeforeScenario @api
   */
  public function bootstrap(): void {
    $driver = $this->getDriver();
    if (!$driver->isBootstrapped()) {
      $driver->bootstrap();
    }
  }

  /**
   * @BeforeScenario
   * Wrap XMLHttpRequest
   */
  public function prepare(BeforeScenarioScope $scope) {
    $this->drupalContext = $scope->getEnvironment()->getContext('Drupal\DrupalExtension\Context\DrupalContext');
  }
}
