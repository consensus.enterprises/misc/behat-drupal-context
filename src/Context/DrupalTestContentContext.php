<?php
namespace Consensus\BehatDrupalContext\Context;

use Consensus\BehatDrupalContext\Context\Traits\Steps\UsersFromFileStepsTrait;
use Consensus\BehatDrupalContext\Context\Traits\Steps\NodesFromFileStepsTrait;
use Consensus\BehatDrupalContext\Context\Traits\Steps\ParagraphsFromFileStepsTrait;
use Consensus\BehatDrupalContext\Context\Traits\Steps\EntityFromFileStepsTrait;

/**
 * Defines steps to create fixtures for development.
 */
class DrupalTestContentContext extends DrupalContentContextBase {

  use UsersFromFileStepsTrait;          # Provides Given users from :file
  use NodesFromFileStepsTrait;          # Provides Given :type content from :file
  use ParagraphsFromFileStepsTrait;     # Provides Given :type paragraph from :file
  use EntityFromFileStepsTrait;         # Provides Given :type entities from :file

}
