# behat-drupal-context

Extended support for creating content in a Drupal site.

See: https://packagist.org/packages/consensus/behat-drupal-context

[![pipeline status](https://gitlab.com/consensus.enterprises/misc/behat-drupal-context/badges/main/pipeline.svg)](https://gitlab.com/consensus.enterprises/misc/behat-drupal-context/-/commits/main) | [![Latest Release](https://gitlab.com/consensus.enterprises/misc/behat-drupal-context/-/badges/release.svg)](https://gitlab.com/consensus.enterprises/misc/behat-drupal-context/-/releases)

## Installation and setup

```yaml
composer require consensus/behat-drupal-context
```

**behat.yml**

```yaml
default:
  suites:
    default:
      contexts:
        - Consensus\BehatDrupalContext\Context\DrupalTestContentContext
        - Consensus\BehatDrupalContext\Context\DrupalDemoContentContext
        - Drupal\DrupalExtension\Context\DrupalContext
```
## DrupalTestContentContext

This package extends the DrupalExtension Behat step definitions for a number of
variations of content creation in a Drupal 8+ site:

1. Create Paragraphs

```
Given a "part_1_paragraph" paragraph named "example_part_1_paragraph":
  | id                | 100         |
  | field_part_1_text | Lorem ipsum |
```

2. Create entities (users, nodes, paragraphs) from a `.table.txt` file:

```
Given users from "features/entities/users.table.txt"
Given "page" content from "features/entities/node-pages.table.txt"
```

Where `.table.txt` is a markdown-formatted table like this:

```
| title              | status | created            | author  | body             |
| Example Basic Page | 1      | 2023-02-02 09:00am | user101 | This is the BODY |
```

3. Create paragraphs in 2 formats, or from a file:

```
Given a "simple_paragraph" paragraph named "example_paragraph_1":
  | field_paragraph_title | Paragraph Title |
  | field_paragraph_desc  | Paragraph Desc  |

Given "simple_paragraph" paragraphs:
  | paragraph_name    | field_simple_paragraph_title | field_simple_paragraph_desc |
  | example_paragraph | Title goes here              | Decription goes here        |

Given "simple_paragraph" paragraphs from "features/entities/paragraphs.table.txt"
```

Where `.table.txt` is a markdown-formatted table like this:

```
| paragraph_name    | field_simple_paragraph_title | field_simple_paragraph_desc |
| example_paragraph | Title goes here              | Decription goes here        |
```

## DrupalDemoContentContext

Create entities (users, nodes, paragraphs) that persist after the Scenario completes (eg. for demo content).

1. Users that persist:

```
Given persistent users:
  | uid | name          | pass | email               | roles          |
  | 101 | user101       | pwd  | user101@example.com |                |
```

2. Nodes that persist:

```
Given persistent "page" content:
  | title              | status | created            | author  | body             |
  | Example Basic Page | 1      | 2023-02-02 09:00am | user101 | This is the BODY |

Given persistent "page" content from "features/entities/node-page.table.txt"
```

3. Paragraphs that persist:

```
Given a "simple_paragraph" persistent paragraph named "example_paragraph_1":
  | field_paragraph_title | Paragraph Title |
  | field_paragraph_desc  | Paragraph Desc  |

Given persistent "simple_paragraph" paragraphs:
  | paragraph_name    | field_simple_paragraph_title | field_simple_paragraph_desc |
  | example_paragraph | Title goes here              | Decription goes here        |

Given persistent "simple_paragraph" paragraphs from "features/entities/paragraphs-simple.table.txt"
```
